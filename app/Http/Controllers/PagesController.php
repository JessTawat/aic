<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PagesController extends Controller
{
    public function index(){
        $title = 'Welcome to to MyEcomm';
        // return view('pages.index', compact('$title'));
        return view('pages.index')->with('title',$title);
    }

    public function about(){
        // $title = 'About Us';
        // return view('pages.about')->with('title', $title);

        $title = 'About Us';
        return view('pages.about')->with('title', $title);
    }

    public function services(){
        $data = array(
            'title' => 'Our Services',
            'services' => ['Web Development', 'Graphic Design', 'SEO'],
            'name' => 'Jess'
        );
        return view('pages.services')->with($data);
    }
}
