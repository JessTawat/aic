@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center">
            <h1>{{$title}}</h1>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti harum sequi repellendus minima, dolore, 
                nobis nostrum iusto, laudantium expedita architecto velit rerum pariatur doloremque magni odit dignissimos 
                numquam voluptates eligendi?    
            </p>

            <p>
                <a class="btn btn-primary btn-lg" role="login" href="{{ route('login') }}">Login</a> 
                <a class="btn btn-success btn-lg" role="register" href="{{ route('register') }}">Register</a>
            </p>
    </div>
@endsection
