@extends('layouts.app')

@section('content')
    <h1>Create a Post</h1>
    {!! Form::open(['action' => 'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{ Form::label('Title') }}
            {{ Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        <div class="form-group">
            {{ Form::textarea('body', '', ['id'=> 'article-ckeditor','class' => 'form-control', 'placeholder' => 'Body'])}}
        </div>
        <div class="form-group">
           {{Form::file('cover_img')}} 
        </div>
        {{Form::Submit('Submit', ['class'=> 'btn btn-primary'])}}
{!! Form::close() !!}
@endsection