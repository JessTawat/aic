@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Posts</h1>
        @if(count($posts) > 0)
            <ul class="list-group">
                @foreach($posts as $post)
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <img style="width: 100%" src="/storage/cover_img/{{$post->cover_img}}" alt="">
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <h3><a href="/posts/{{$post->id}}">{{$post->title}}</a></h3>
                                {{-- With formatting --}}
                                {{-- <small>Written on {{$post->created_at->format('M/d/y')}}</small> --}}
                                <small>Written on {{$post->created_at}}</small>
                            </div>
                        </div>
                    </li>        
                @endforeach
                {{-- pagination --}}
                {{$posts->links()}}
            @else
                <h2>No Record Found!</h2>
            @endif
            </ul>
    </div>
@endsection