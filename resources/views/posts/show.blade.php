@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-default">Go Back</a>
        <h1>{{$post->title}}</h1>
        <img style="width: 100%" src="/storage/cover_img/{{$post->cover_img}}" alt="">
        <br>
        <div>
            {!!$post->body!!}
        </div>
    <small>Written on {{$post->created_at}}</small>
    <hr>
    @if(!Auth::guest())
        @if(Auth::user()->id == $post->user_id)
            <a href="/posts/{{$post->id}}/edit" class="btn btn-default">Edit</a>

            {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST','onsubmit'=>'return confirmDelete()','class' => 'float-right'])!!}
            {!!Form::hidden('_method', 'DELETE')!!}
            {!!Form::Submit('Delete',['class' => 'btn btn-danger'])!!}
            {!!Form::close()!!}
        @endif
    @endif
@endsection
