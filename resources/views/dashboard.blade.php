@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in!
                </div>
            </div>
            <div class="m-2">
                <a href="/posts/create" class="btn btn-primary">Create Post</a>
                <div class="m-2">
                    <h3>Your Blog Posts</h3>
                    <table class="table table-striped">
                        <tr>
                            <th>Title</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach ($posts as $post)
                            <tr>
                                <td>{{$post->title}}</td>
                                <td>
                                    <a href="/posts/{{$post->id}}/edit" class="btn btn-default">Edit</a>
                                </td>
                                <td>
                                    {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST','onsubmit'=>'return confirmDelete()','class' => 'float-right'])!!}
                                    {!!Form::hidden('_method', 'DELETE')!!}
                                    {!!Form::Submit('Delete',['class' => 'btn btn-danger'])!!}
                                    {!!Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script>
        function confirmDelete() {
        var result = confirm('Are you sure you want to delete?');
        
        if (result) {
                return true;
            } else {
                return false;
            }
        }   
        </script>
